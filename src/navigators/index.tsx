import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeeDetailScreen, EmployeesScreen, UserProfile } from '../screens';

const Drawer = createDrawerNavigator();

export function MainNavigator() {
  return (
    <Drawer.Navigator initialRouteName="Employees">
      <Drawer.Screen name="Employees" component={EmployeesScreen} />
      <Drawer.Screen name="EmployeeDetail" component={EmployeeDetailScreen} />
      <Drawer.Screen name="UserProfile" component={UserProfile} />
    </Drawer.Navigator>
  );
}
